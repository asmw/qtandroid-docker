#!/bin/bash
set -e
set -u

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

source $SCRIPTPATH/android-settings.inc

ANDROID_HOME=${ANDROID_HOME:-~/android}

DOWNLOAD_DIR=${DOWNLOAD_DIR:-$ANDROID_HOME/downloads}
if [ ! -d $DOWNLOAD_DIR ]; then mkdir -p $DOWNLOAD_DIR; fi
pushd $DOWNLOAD_DIR
wget -c $REPO_URL/$SDK_TOOLS
wget -c $REPO_URL/$BUILD_TOOLS
wget -c $REPO_URL/$PLATFORM_TOOLS
wget -c $REPO_URL/$PLATFORM
wget -c $REPO_URL/$NDK
popd

SDK_HOME=$ANDROID_HOME/sdk
mkdir -p $SDK_HOME

if [ -z "${SKIP_NDK:-}" ]; then
cd $ANDROID_HOME
tar xvf $DOWNLOAD_DIR/$NDK
ln -s $(ls --sort time | grep ndk | head -n1) ndk
fi

mkdir -p $SDK_HOME
cd $SDK_HOME
unzip -o $DOWNLOAD_DIR/$PLATFORM_TOOLS
unzip -o $DOWNLOAD_DIR/$SDK_TOOLS

BUILD_TOOLS_DIR=$SDK_HOME/build-tools
mkdir -p $BUILD_TOOLS_DIR
cd $BUILD_TOOLS_DIR
if [ ! -d $BUILD_TOOL_VERSION_DIR ]; then
  unzip -o $DOWNLOAD_DIR/$BUILD_TOOLS
  mv $(ls --sort time | head -n1) $BUILD_TOOL_VERSION_DIR
fi

PLATFORM_DIR=$SDK_HOME/platforms/
mkdir -p $PLATFORM_DIR
cd $PLATFORM_DIR
if [ ! -d $PLATFORM_VERSION_DIR ]; then
  unzip -o $DOWNLOAD_DIR/$PLATFORM
  mv $(ls --sort time | head -n1) $PLATFORM_VERSION_DIR
fi
