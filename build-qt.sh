#!/bin/bash
NDK=${ANDROID_NDK:-~/android/ndk}
SDK=${ANDROID_SDK:-~/android/sdk}
PREFIX=${QT_PREFIX:-/opt/Qt${QT_VERSION:-}}

./configure \
	-xplatform android-clang \
	--disable-rpath \
	-nomake tests \
	-nomake examples \
	-android-ndk $NDK \
	-android-sdk $SDK \
	-android-ndk-host linux-x86_64\
       	-skip qttranslations \
	-skip qtserialport \
	-skip qttools \
	-no-warnings-are-errors \
	-opensource \
	-confirm-license \
	-prefix $PREFIX \
	-android-abis armeabi-v7a,arm64-v8a

make -j$(nproc)
make install
