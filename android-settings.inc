REPO_URL="https://android-rebuilds.beuc.net/dl/repository/"

SDK_TOOLS="sdk-repo-linux-tools-26.1.1.zip"
PLATFORM_TOOLS="sdk-repo-linux-platform-tools-eng.10.0.0_r14.zip"

BUILD_TOOLS="sdk-repo-linux-build-tools-eng.10.0.0_r36.zip"
BUILD_TOOL_VERSION_DIR="29.0.3"

PLATFORM="sdk-repo-linux-platforms-eng.10.0.0_r36.zip"
PLATFORM_VERSION_DIR="android-29"

NDK="android-ndk-r20b-linux-x86_64.tar.bz2"
