#!/bin/bash
NDK=${ANDROID_NDK:-~/android/ndk}
SDK=${ANDROID_SDK:-~/android/sdk}
PREFIX=${QT_PREFIX:-/opt/Qt${QT_VERSION:-}}

git clone git://code.qt.io/qt/qt5.git
cd qt5
git checkout ${QT_TAG:-5.15.0}
perl init-repository
