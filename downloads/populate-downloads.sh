#!/bin/bash
set -e
set -u

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
source $SCRIPTPATH/../android-settings.inc

wget -c $REPO_URL/$SDK_TOOLS
wget -c $REPO_URL/$BUILD_TOOLS
wget -c $REPO_URL/$PLATFORM_TOOLS
wget -c $REPO_URL/$PLATFORM
wget -c $REPO_URL/$NDK
