FROM debian:stable-slim as android
WORKDIR /root/
COPY downloads/* /tmp/
ADD install-android-tools.sh .
ADD android-settings.inc .
ENV ANDROID_HOME /opt/android
ENV DOWNLOAD_DIR /tmp/
RUN apt-get update && apt-get install -y \
    wget \
    bzip2 \
    unzip \
    && \
    ./install-android-tools.sh

FROM debian:stable as qtandroid-build
WORKDIR /root/
ADD build-qt.sh .
ADD get-qt.sh .
ADD android-settings.inc .
COPY --from=android /opt/android /opt/android
ENV ANDROID_NDK /opt/android/ndk
ENV ANDROID_SDK /opt/android/sdk
ENV QT_TAG 5.15.0
RUN apt-get update && apt-get install -y \
    openjdk-11-jdk \
    build-essential \
    git \
    perl \
    python
RUN ./get-qt.sh
RUN cd qt5 && ../build-qt.sh

FROM debian:stable as qtandroid
VOLUME /data
WORKDIR /data
COPY --from=qtandroid-build /opt /opt
ENV ANDROID_NDK /opt/android/ndk
ENV ANDROID_SDK /opt/android/sdk
ENTRYPOINT "/bin/bash"
